# How to run

1. You'll need an api key from [Flickr](https://www.flickr.com/services/apps/create/apply)
2. Open up server folder and create .env file and paste Your api key example **API_KEY='your_key'**
3. After that install all modules by running **npm install**
4. You can run now Backend with **nodemon** command and server side setup is done
5. Head back to client folder
6. Install all modules with **npm install**
7. Next run frontend with in the development mode **npm start**
8. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
