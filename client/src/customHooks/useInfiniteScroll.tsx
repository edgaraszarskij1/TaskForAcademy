import { useState, useEffect } from "react";

export const useInfiniteScroll = (fetchNextPage: () => Promise<void> ) => {
  const [isFetching, setIsFetching] = useState<any>(false);
  
  const isScrolling = () => {
    if (
      window.innerHeight + document.documentElement.scrollTop !==
        document.documentElement.offsetHeight ||
      isFetching
    )
      return;
    setIsFetching(true);
    fetchNextPage().then(()=> setIsFetching(false))
  };

  useEffect(() => {
    window.addEventListener("scroll", isScrolling);
    return () => window.removeEventListener("scroll", isScrolling);
  }, [isScrolling]);

  useEffect(()=>{
    setIsFetching(true)
    fetchNextPage().then(()=> setIsFetching(false))
  },[])

  return [isFetching, setIsFetching];
};
