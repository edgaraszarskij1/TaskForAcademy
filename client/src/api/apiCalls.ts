const ApiUrl = "http://localhost:3001/";
export const getInitialPictures = async (page: number) => {
  const url = `${ApiUrl}services/rest/?method=flickr.photos.getRecent&per_page=16&page=${page}&format=json&nojsoncallback=1`;
  const response = await fetch(url, {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });
  if (!response.ok) {
    throw Error(response.statusText);
  }
  const data = await response.json();
  return data;
};

export const getPictureOwner = async (ownerId: string) => {
  const url = `${ApiUrl}services/rest/?method=flickr.profile.getProfile&user_id=${ownerId}&format=json&nojsoncallback=1`;
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(response.statusText);
  }
  const data = await response.json();
  return data;
};
