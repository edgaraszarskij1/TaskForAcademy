import React, { useEffect, useState } from "react";
import { getPictureOwner } from "../../api/apiCalls";
import { PicturesInterface } from "../../interfaces/PicturesInterfaces";
import { Profile } from "../../interfaces/PictureOwnerInterface";
import "../photos.css";

const Picture = (props: {
  picture: PicturesInterface;
  addToFavourite: (picture:PicturesInterface)=>void;
  removeFromFavourite: (pictureId:string)=>void;
}): JSX.Element => {
  const [hovered, setHovered] = useState<boolean>(false);
  const [pictureOwner, setPictureOwner] = useState<Profile>();

  const onPictureHover = () => {
    setHovered(true);
  };

  useEffect(() => {
    getPictureOwner(props.picture.owner).then((data) => setPictureOwner(data));
  }, []);

  const handleMouseLeave = () => {
    setHovered(false);
  };

  return (
    <div
      className="item"
      onMouseEnter={onPictureHover}
      onMouseLeave={handleMouseLeave}
    >
      <img
       
        alt=""
        src={`https://live.staticflickr.com/${props.picture.server}/${props.picture.id}_${props.picture.secret}_n.jpg`}
      />
      <div
        className={hovered ? "hover-image-visible" : "hover-image-invisible"}
      >
        <p className="title">
          {props.picture.title ? props.picture.title : "No Title"}
        </p>
        <p className="owner">
          {pictureOwner?.profile.last_name
            ? pictureOwner.profile.last_name
            : "No Initials"}{" "}
          {pictureOwner?.profile.last_name}
        </p>
        {props.picture.isFavourite ? (
          <button
            className="button green"
            onClick={() => props.removeFromFavourite(props.picture.id)}
          >
            Unfavorite
          </button>
        ) : (
          <button
            className="button"
            onClick={() => props.addToFavourite(props.picture)}
          >
            Favorite
          </button>
        )}
      </div>
    </div>
  );
};

export default Picture;
