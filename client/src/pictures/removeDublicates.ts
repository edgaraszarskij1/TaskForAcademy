import { PicturesInterface } from "../interfaces/PicturesInterfaces";

export const removeDublicates = (array: PicturesInterface[]) => {
  const removedDublicates = array.filter(
    (picture, index, self) =>
      index ===
      self.findIndex(
        (pic) => pic.id === picture.id && pic.owner === picture.owner
      )
  );
  return removedDublicates;
};
