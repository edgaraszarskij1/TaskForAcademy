import { PicturesInterface } from "../interfaces/PicturesInterfaces";

export const addToFavourites = (
  array: PicturesInterface[],
  id: string,
  favourite: boolean
) => {
  const addToFavourites = array.map((picture) =>
    picture.id === id ? { ...picture, isFavourite: favourite } : picture
  );
  return addToFavourites;
};
