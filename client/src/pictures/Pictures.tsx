import React, { useState, useEffect } from "react";
import { getInitialPictures } from "../api/apiCalls";
import { useInfiniteScroll } from "../customHooks/useInfiniteScroll";
import { PicturesInterface } from "../interfaces/PicturesInterfaces";
import { addToFavourites } from "./addToFavourites";
import {removeDublicates} from './removeDublicates'
import Picture from "./picture/Picture";
import "./photos.css";

const Pictures = (): JSX.Element => {
  const [pictures, setPictures] = useState<PicturesInterface[]>([]);
  const [page, setPage] = useState<number>(0);

  const [favourite, setFavourite] = useState<PicturesInterface[]>(
    JSON.parse(localStorage.getItem("favourite") || "[]")
  );

  const nextPage = 1;

   useInfiniteScroll(()=>  getInitialPictures(page).then((data) => {
    setPage((prevState) => prevState + nextPage);
    if (favourite.length > 0 && pictures.length === 0) {
      setPictures(removeDublicates([...favourite,...data.photos.photo]));
    } else if (pictures.length === 0) {
      setPictures(data.photos.photo);
    } else {
    setPictures(removeDublicates([...pictures, ...data.photos.photo]));
   }
  }));

  const addToFavourite = (favouriteObj: PicturesInterface) => {

    setPictures(addToFavourites(pictures, favouriteObj.id, true));

    setFavourite((prevState: PicturesInterface[]) => {
      const newFavourite = [
        ...prevState,
        { ...favouriteObj, isFavourite: true },
      ];
      window.localStorage.setItem("favourite", JSON.stringify(newFavourite))
      return newFavourite;
    });
  };

  const removeFromFavourite = (id: string) => {
    setPictures(addToFavourites(pictures, id, false));

    setFavourite((prevState) => {
      const removeFavourite = prevState.filter(
        (favourite) => favourite.id !== id
      );
      window.localStorage.setItem("favourite", JSON.stringify(removeFavourite));
      return removeFavourite;
    });
  };
  

  return (
    <div className="container">
      {pictures.length > 0 &&
        pictures.map((picture) => (
          <Picture
            key={picture.id}
            picture={picture}
            addToFavourite={addToFavourite}
            removeFromFavourite={removeFromFavourite}
          />
        ))}
    </div>
  );
};

export default Pictures;
