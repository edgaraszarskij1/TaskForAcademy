export interface PicturesInterface {
  id: string;
  server: string;
  secret: string;
  title: string;
  owner: string;
  isFavourite?: boolean;
}
