export interface PictureOwner {
  first_name: string;
  last_name: string;
}

export interface Profile {
  profile: PictureOwner;
}
