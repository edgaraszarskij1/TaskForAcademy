var express = require('express')
var cors = require('cors')
const { createProxyMiddleware } = require('http-proxy-middleware')
require('dotenv').config();
var app = express();
const port = 3001
app.use(cors())
var API_KEY = process.env.API_KEY


const flickr = createProxyMiddleware({
    target: 'https://www.flickr.com',
    changeOrigin: true, 
    logLevel: 'debug',
    onProxyReq: (proxyReq, req, res) => {
      proxyReq.path += '&api_key=' + API_KEY;
    },
  })

  app.use('/',flickr)
  app.listen(port)